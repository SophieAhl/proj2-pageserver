#Sophie Ahlberg
#322 Introduction to Software Engineering
#Assignment 2


from flask import Flask, abort #Include abort to call on the error pages
import logging
import re
app = Flask(__name__)

@app.route('/<name>') #Name is not defined here, but will change with the if and else statements

def hello(name):

	regex = re.compile('[@_!#$%^&*()<>|?/}{~:.,123456789]') #This regular expression formula will compile all the special characters which will cause the 403 error
 
    #Source: https://www.geeksforgeeks.org/python-program-check-string-contains-special-character/

	if (regex.search(name) == None) and (name == "html"): #If there is no special character and the name is correct, take user to the correct page
		return "This is Sophie's webpage"

	elif (regex.search(name) == None) and (name!= "html"): #If there is no special characters, but the name is not defined correctly. Call upon error 404
		abort(404)
 
	else:
		abort(403) #Name has special characters. Print 403 error (EXCEPT FOR FORWARD SLASHES! I was unable to solve that dilemma)

if __name__ == "__main__":
    app.run(debug=True,host='0.0.0.0')











    
